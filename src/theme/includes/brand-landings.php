<?php
/**
 * Shortcode de breadcrumbs en landing page de marcas
 */
add_shortcode( 'gv-brand-landing-breadcrumbs', 'gv_brand_landing_breadcrumbs' );
function gv_brand_landing_breadcrumbs( $atts, $content = null ) {
    global $post; 
    $title = get_the_title();
    $url = get_the_permalink();
    ?>
    <div class="breadcrumbs">
        <div id="breadcrumb">
            <a href="<?php echo home_url(); ?>" style="text-transform: uppercase;">Garantía Verde</a>
            <span class="delimeter"><i class="et-icon et-right-arrow"></i></span>
            <?php if($post->post_parent):
                $parent_title = get_the_title($post->post_parent);
                $parent_url = get_the_permalink($post->post_parent);
            ?>
                <a href="<?php echo $parent_url; ?>" style="text-transform: uppercase;"><?php echo $parent_title ?></a>
                <span class="delimeter"><i class="et-icon et-right-arrow"></i></span>
            <?php endif; ?>
            <a href="<?php echo $url; ?>" style="text-transform: uppercase;"><?php echo $title; ?></a>
            <span class="delimeter"><i class="et-icon et-right-arrow"></i></span>
        </div>
    </div>
    <?php
}


/**
 * Cambiar funcion de links de productos
 */
function gv_brand_landing_scripts() {
    if(!is_single() && get_post_type() != 'brand-landing') return;
    ?>
    <script>
        (function ($) {
            $brandLandProducts = $('.brand-landing-products');
            if($brandLandProducts.length){
                $mainURL = $brandLandProducts.find('.swiper-entry .swiper-slide .product-title a, a.product-content-image');
                $disabledURLs = $brandLandProducts.find('.swiper-entry .swiper-slide .products-page-cats a, .swiper-entry .swiper-slide .products-page-brands a');
                $mainURL.click(function(e){
                    e.preventDefault();
                    $quickview = $(this).parents('.content-product').find('span.show-quickly');
                    $quickview.click();
                }).removeAttr('href');
                $disabledURLs.removeAttr('href');
            }
        })(jQuery);
    </script>
    <?php
}
add_action( 'wp_footer', 'gv_brand_landing_scripts', 1, 1 );

/**
 * Agregar estilos
 */

function brand_landing_styles() {
    if(!is_single() && get_post_type() != 'brand-landing') return;
    global $post;
    $main_color = get_post_meta($post->ID, 'main_color', true) ? get_post_meta($post->ID, 'main_color', true) : '#000000';
    $text_color = get_post_meta($post->ID, 'text_color', true) ? get_post_meta($post->ID, 'text_color', true) : '#FFFFFF';
    ?>
    <style>
        .brand-category-carousel .vc_figure-caption{
            font-size: 1em;
        }

        .header-top, .footer-bottom, .sticky-on .header-top, .brand-landing-header{
            background-color: <?php echo $main_color ?> !important;
            color: <?php echo $text_color ?> !important;
        }

        .breadcrumbs, .breadcrumbs a{
            color: <?php echo $text_color ?> !important;
        }

        .breadcrumbs a:hover{
            color: <?php echo $text_color ?> !important;
            opacity: 0.8 !important;

        }

        .et-footers-wrapper{
            border-top: 4px solid <?php echo $main_color ?>;
        }

        .post-header, .page-heading, .share-post, .related-posts, .posts-navigation{
            display: none !important;
        }
    
    </style>
    <?php
}
add_action( 'wp_head', 'brand_landing_styles' );

 /**
  * Algoritmo para detectar cruces
  */
function brand_landing_admin_menu()
{
    add_submenu_page(
        'edit.php?post_type=brand-landing',
        __( 'Marcas y categorías', 'textdomain' ),
        __( 'Marcas y categorías', 'textdomain' ),
        'manage_options',
        'brand_landing_categories',
        'brand_landing_categories_page_contents'
    );
}

add_action('admin_menu', 'brand_landing_admin_menu');

function brand_landing_categories_page_contents()
{
    $brands = get_terms( array(
        'taxonomy' => 'brand',
        'hide_empty' => false,
    ) );
    
?>
    <div class="wrap">
        <h1 class="wp-heading-inline">MARCAS Y CATEGORÍAS RELACIONADAS</h1>
        <?php foreach($brands as $brand):
            $brand_categories = array();
            $product_ids = get_posts( array(
                'post_type' => 'product',
                'numberposts' => -1,
                'fields' => 'ids',
                'tax_query' => array(
                array(
                    'taxonomy' => 'brand',
                    'terms' => $brand->term_id,
                    'operator' => 'IN',
                )
                ),
            ) );

            foreach($product_ids as $product_id){
                $product_categories = wp_get_post_terms($product_id,'product_cat',array('fields'=>'names'));
                $brand_categories = array_unique(array_merge($brand_categories, $product_categories));
            }        
        ?>
            <h2>Marca: <?php echo $brand->name; ?></h2>
            <ul>
                <?php foreach($brand_categories as $brand_category): ?>
                    <li><?php echo $brand_category; ?></li>
                <?php endforeach; ?>
            </ul>
            <hr>

        <?php endforeach; ?>
    </div>

<?php

}

/**
 * Custom post type Marcas
 */
add_action( 'init', 'gv_custom_post_type_brand_landing', 0 );
function gv_custom_post_type_brand_landing() {
	$labels = array(
		'name'                => __( 'Landings de Marcas' ),
		'singular_name'       => __( 'Landing'),
		'menu_name'           => __( 'Landings de Marcas'),
		'parent_item_colon'   => __( 'Landing padre'),
		'all_items'           => __( 'Todas las Landings'),
		'view_item'           => __( 'Ver Landing'),
		'add_new_item'        => __( 'Agregar Nueva Landing'),
		'add_new'             => __( 'Agregar Landing'),
		'edit_item'           => __( 'Editar Landing'),
		'update_item'         => __( 'Actualizar Landing'),
		'search_items'        => __( 'Buscar Landing'),
		'not_found'           => __( 'Landing no encontrada'),
		'not_found_in_trash'  => __( 'Landing no encontrada en papelera')
	);
	$args = array(
		'label'               => __( 'brand-landing'),
		'description'         => __( 'Landing pages para Marcas'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'page-attributes' ),
		'public'              => true,
		'hierarchical'        => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'has_archive'         => false,
		'can_export'          => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'rewrite'             => array( 'slug' => 'marcas' ),
        'menu_icon'           => 'dashicons-desktop'
    );
    register_post_type( 'brand-landing', $args );
    
}

/**
 * Agregar selector de color principal
 */
add_action( 'admin_menu', 'gv_brand_landing_meta_box' );
 
function gv_brand_landing_meta_box() {
	add_meta_box('brand-landing-options', // meta box ID
		'Configuración de Landing Page', // meta box title
		'gv_brand_landing_print_box', // callback function that prints the meta box HTML 
		'brand-landing', // post type where to add it
		'normal', // priority
		'high' ); // position
}

add_action( 'admin_enqueue_scripts', 'gv_brand_landing_admin_scripts');
function gv_brand_landing_admin_scripts( $hook ) {
    wp_enqueue_style( 'wp-color-picker');
    wp_enqueue_script( 'wp-color-picker');
}
 
/*
 * Meta Box HTML
 */
function gv_brand_landing_print_box( $post ) {
	$custom = get_post_custom( $post->ID );
		$main_color = ( isset( $custom['main_color'][0] ) ) ? $custom['main_color'][0] : '';
		$text_color = ( isset( $custom['text_color'][0] ) ) ? $custom['text_color'][0] : '';
		wp_nonce_field( 'gv_brand_landing_metabox', 'gv_brand_landing_nonce' );
		?>
		<script>
		jQuery(document).ready(function($){
		    $('.color_field').each(function(){
        		$(this).wpColorPicker();
    		    });
		});
		</script>
		<div class="pagebox">
		    <p><?php esc_attr_e('Color Principal de landing.', 'xstore' ); ?></p>
		    <input class="color_field" type="text" name="main_color" value="<?php esc_attr_e( $main_color ); ?>"/>
		</div>
		<div class="pagebox">
		    <p><?php esc_attr_e('Color Principal de texto.', 'xstore' ); ?></p>
		    <input class="color_field" type="text" name="text_color" value="<?php esc_attr_e( $text_color ); ?>"/>
		</div>
		<?php
}
 
/*
 * Save Meta Box data
 */
add_action('save_post', 'gv_brand_landing_save');
 
function gv_brand_landing_save( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }
    if( !current_user_can( 'edit_pages' ) ) {
        return;
    }
    if ( !isset( $_POST['main_color'] ) || !wp_verify_nonce( $_POST['gv_brand_landing_nonce'], 'gv_brand_landing_metabox' ) ) {
        return;
    }
    $main_color = (isset($_POST['main_color']) && $_POST['main_color']!='') ? $_POST['main_color'] : '';
    update_post_meta($post_id, 'main_color', $main_color);

    if ( !isset( $_POST['text_color'] ) || !wp_verify_nonce( $_POST['gv_brand_landing_nonce'], 'gv_brand_landing_metabox' ) ) {
        return;
    }
    $text_color = (isset($_POST['text_color']) && $_POST['text_color']!='') ? $_POST['text_color'] : '';
    update_post_meta($post_id, 'text_color', $text_color);
}

/**
 * Agregar filtro para slider de productos
 */
add_filter('gv_get_products_slider_args', 'gv_brand_landing_category_args', 10, 2);
function gv_brand_landing_category_args($args, $title){
    if($title != 'brand_landing_category') return $args;
    global $post;
    $post_parent = get_post($post->post_parent);
    if($post_parent){
        $brand = $post_parent->post_name;
        $original_query = isset($args['tax_query']) ? $args['tax_query'] : array();
        $brand_query  = array(
            'relation' => 'AND',
            array(
                'taxonomy'         => 'brand',
                'terms'            => $brand,
                'field'            => 'slug'
            )
        );
        $args['tax_query']= array(
            'relation' => 'AND',
            $original_query,
            $brand_query
        );
    };
    return $args;
}