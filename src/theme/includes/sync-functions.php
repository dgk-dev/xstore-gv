<?php
/**
 * Funciones para importación y exportación de CSV con WP ALL IMPORT/EXPORT
 */

function gv_get_regular_price($product_id){
	return get_post_meta( $product_id, '_regular_price', true);
}

function gv_get_item_tax($order_line_id){
    $total_tax = wc_get_order_item_meta($order_line_id, '_line_subtotal_tax', true);
    $qty = wc_get_order_item_meta($order_line_id, '_qty', true);
    return round($total_tax/$qty,2);

}
function gv_get_items_subtotal($order_line_id){
    $subtotal = wc_get_order_item_meta($order_line_id, '_line_total', true);
    $tax = wc_get_order_item_meta($order_line_id, '_line_tax', true);
    return $subtotal+$tax;
}

function gv_get_discount_amount($product_id){
    $regular_price = get_post_meta( $product_id, '_regular_price', true);
    $sale_price = get_post_meta( $product_id, '_sale_price', true) ? get_post_meta( $product_id, '_sale_price', true) : $regular_price;
    return $sale_price < $regular_price ? ($regular_price - $sale_price): '';
}

// Copiar CSV a carpeta sync
add_filter( 'wp_all_export_export_file_name', 'gv_copy_csv_to_path', 10, 2 );
function gv_copy_csv_to_path( $file_path, $export_id ){
    $path_array = explode('/',$file_path);
    if(strpos(end($path_array),'current-') !== false) return $file_path;

    $new_file_path = WP_CONTENT_DIR.'/uploads/sync/orders/'.end($path_array);
    $copy = copy($file_path, $new_file_path);

    return $file_path;
}

// Borrar archivos despues de importar
add_action('pmxi_after_xml_import', 'gv_delete_files_after_import', 10, 2);
function gv_delete_files_after_import($import_id, $import){
	switch($import_id){
		case 8:
            unlink(WP_CONTENT_DIR.'/uploads/sync/products_gv.csv');
        break;
		case 9:
            unlink(WP_CONTENT_DIR.'/uploads/sync/stock_gv.csv');
        break;
	}
}
