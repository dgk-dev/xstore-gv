<?php
//Whatsapp
add_action('wp_footer','gv_add_footer_whatsapp');
function gv_add_footer_whatsapp(){
	$tel = "5214622103231";

	$url = "https://wa.me/${tel}";
	$img = get_stylesheet_directory_uri().'/img/whatsapp-icon.svg';
	echo "<div id='float-whatsapp'>";
	echo "<a href=${url} target='_blank'>";
	echo "<img width='40' height='40' id='float-whatsapp-img' src='${img}' alt='whatsapp-icon' />";
	echo " </a>";
	echo "</div>";
}

//Facebook messenger plugin
add_action( 'wp_head', 'gv_facebook_messenger', 10 );
function gv_facebook_messenger() {
    ?>
      <!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
      window.fbAsyncInit = function() {
        FB.init({
          xfbml : true,
          version : 'v7.0'
        });
      };
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/es_LA/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your Chat Plugin code -->
      <div class="fb-customerchat"
      attribution=setup_tool
      page_id="2339776372947253"
      theme_color="#13cf13"
      logged_in_greeting="¡Hola! ¿Cómo te podemos ayudar?"
      logged_out_greeting="¡Hola! ¿Cómo te podemos ayudar?">
      </div>
    <?php
}
