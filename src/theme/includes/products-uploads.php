<?php

function wc_include_uploadscript() {
	/*
	 * I recommend to add additional conditions just to not to load the scipts on each page
	 * like:
	 * if ( !in_array('post-new.php','post.php') ) return;
	 */
	if ( ! did_action( 'wp_enqueue_media' ) ) {
		wp_enqueue_media();
	}
 
 	wp_enqueue_script( 'wcuploadscript', get_stylesheet_directory_uri() . '/admin/js/product-uploads.js', array('jquery'), null, false );
}
 
add_action( 'admin_enqueue_scripts', 'wc_include_uploadscript' );

/*
 * @param string $name Name of option or name of post custom field.
 * @param string $value Optional Attachment ID
 * @return string HTML of the Upload Button
 */
function wc_file_uploader_field( $name, $value = '') {
	$file= ' button">Subir archivo';
	$display = 'none'; // display state ot the "Remove image" button
 
	if( $file_attributes = get_post_meta( intval($value) , '_wp_attached_file', true) ) {
		$file = '">'. $file_attributes;
		$display = 'block';
	} 
 
	return '
	<div>
		<a href="#" class="wc_upload_file' . $file . '</a>
		<input type="hidden" name="' . $name . '" id="' . $name . '" value="' . esc_attr( $value ) . '" />
		<a href="#" class="wc_remove_file" style="color:red;display:block;display:' . $display . '">Remover archivo</a>
	</div>';
}

/*
 * Add a meta box
 */
add_action( 'admin_menu', 'wc_upload_meta_box_add' );
 
function wc_upload_meta_box_add() {
	add_meta_box('wcuploaddiv', // meta box ID
		'Archivo PDF (ficha técnica)', // meta box title
		'wc_upload_print_box', // callback function that prints the meta box HTML 
		'product', // post type where to add it
		'normal', // priority
		'high' ); // position
}
 
/*
 * Meta Box HTML
 */
function wc_upload_print_box( $post ) {
	$meta_key = 'gv_ficha_tecnica';
	echo wc_file_uploader_field( $meta_key, get_post_meta($post->ID, $meta_key, true) );
}
 
/*
 * Save Meta Box data
 */
add_action('save_post', 'wc_upload_save');
 
function wc_upload_save( $post_id ) {
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
		return $post_id;
 
	$meta_key = 'gv_ficha_tecnica';
 
	update_post_meta( $post_id, $meta_key, sanitize_text_field( $_POST[$meta_key] ) );
 
	// if you would like to attach the uploaded image to this post, uncomment the line:
	// wp_update_post( array( 'ID' => $_POST[$meta_key], 'post_parent' => $post_id ) );
 
	return $post_id;
}

function etheme_product_share() {
	if ( etheme_get_option( 'share_icons' ) ):
		global $product; ?>
		<div class="product-share">
			<?php echo do_shortcode( '[share title="' . __( 'Share: ', 'xstore' ) . '" text="' . $product->get_title() . '"]' ); ?>
		</div>
	<?php endif;
	do_action('etheme_after_product_share');
}

add_action('etheme_after_product_share', 'gv_set_ficha_tecnica', 10);
function gv_set_ficha_tecnica(){
	global $product;
	$fileID = get_post_meta($product->get_ID(), 'gv_ficha_tecnica', true);
	if($fileID):
	$attachmentURL = wp_get_attachment_url( $fileID );
	?>
	<a href="<?php echo $attachmentURL ?>" target="_BLANK" class="btn active" style="margin:10px 0">Ficha técnica</a>
	<?php
	endif;
}