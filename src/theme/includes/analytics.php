<?php
//Google analytics tag
add_action( 'wp_head', 'gv_google_analytics', 7 );
function gv_google_analytics() {
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-167974712-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-167974712-1');
    </script>
    <?php
}

/**
 * Funciones avanzadas de google analytics para ecommerce
 * 
 */

//Transacción completa
add_action('woocommerce_thankyou', 'gv_ga_add_transaction', 10, 1);
function gv_ga_add_transaction($order_id){
  $order = wc_get_order( $order_id );

  if ( ! $order || get_post_meta($order->get_id(), '_ga_tracked', true) == 1) {
    return;
  }
  $items = array();
  if ( $order->get_items() ) {
    foreach ( $order->get_items() as $item ) {
      $items []= gv_ga_add_item( $item );
    }
  }

  $ga_code = "gtag( 'event', 'purchase',  {";
  $ga_code .= "'transaction_id': '" . esc_js( $order->get_id() ) . "',";
  $ga_code .= "'value': '" . esc_js( $order->get_total() ) . "',";
  $ga_code .= "'currency': '" . esc_js( $order->get_currency() ) . "',";
  $ga_code .= "'items':" . json_encode($items);
  $ga_code .= "} );";


  // Mark the order as tracked.
  update_post_meta( $order_id, '_ga_tracked', 1 );
  echo "<script>".$ga_code."</script>";
}

function gv_ga_add_item($item){
  $product = $item->get_product();
  $categories = array();
  $category_terms =  get_the_terms($item->get_product_id(), 'product_cat');
  foreach($category_terms as $category){
    $categories []= $category->name;
  }

  $brands = array();
  $brand_terms =  get_the_terms($product->get_id(), 'brand') ? get_the_terms($product->get_id(), 'brand') : array();
  foreach($brand_terms as $brand){
    $brands []= $brand->name;
  }

  $_item = array(
    'id' => $product->get_sku() ? $product->get_sku() : ( '#' . $item->get_product_id() ),
    'name' => $item->get_name(),
    'brand' => implode(', ', $brands),
    'category' => implode(', ', $categories),
    'quantity' =>  $item->get_quantity(),
    'price' => $item->get_total()
  );

  return $_item;
}


// //Agregar al carrito
add_action( 'woocommerce_after_add_to_cart_button', 'gv_ga_add_to_cart');
function gv_ga_add_to_cart(){
  global $product;
    
  $categories = array();
  $category_terms =  get_the_terms($product->get_id(), 'product_cat');
  foreach($category_terms as $category){
    $categories []= $category->name;
  }

  $brands = array();
  $brand_terms =  get_the_terms($product->get_id(), 'brand') ? get_the_terms($product->get_id(), 'brand') : array();
  foreach($brand_terms as $brand){
    $brands []= $brand->name;
  }

  $ga_code = "gtag( 'event', 'add_to_cart', {";
  $ga_code .= "'items': [{";
  $ga_code .= "'id': '" . esc_js( $product->get_sku() ? $product->get_sku() : ( '#' . $product->get_id() ) ) . "',";
  $ga_code .= "'name': '" . esc_js( $product->get_title() ) . "',";
  $ga_code .= "'brand': '" . esc_js( implode(', ', $brands) ) . "',";
  $ga_code .= "'category': '" . esc_js( implode(', ', $categories) ) . "',";
  $ga_code .= "'price': '" . esc_js( $product->get_price() ) . "',";
  $ga_code .= "'quantity': $( 'input.qty' ).length ? $( 'input.qty' ).val() : '1'".",";
  $ga_code .= "} ] } );";

  $selector = '.single_add_to_cart_button';
  gv_ga_set_add_to_cart_code($selector, $ga_code);
}

add_action( 'woocommerce_loop_add_to_cart_link', 'filter_wc_loop_add_to_cart_link', 10, 3 );
function filter_wc_loop_add_to_cart_link( $button_html, $product, $args ) {
    if( $product->supports( 'ajax_add_to_cart' ) ) {
        $search_string  = 'data-product_sku';
        
        // Insert custom product data as data tags
        
        $categories = array();
        $category_terms =  get_the_terms($product->get_id(), 'product_cat');
        foreach($category_terms as $category){
            $categories []= $category->name;
        }

        $brands = array();
        $brand_terms =  get_the_terms($product->get_id(), 'brand') ? get_the_terms($product->get_id(), 'brand') : array();
        foreach($brand_terms as $brand){
            $brands []= $brand->name;
        }
        $replace_string = sprintf(
            'data-ga_id="%s" data-ga_name="%s" data-ga_brand="%s" data-ga_category="%s" data-ga_price="%s" data-ga_quantity="%s" %s',
            $product->get_sku() ? $product->get_sku() : ( '#' . $product->get_id() ), // product name
            $product->get_title(),
            implode(', ', $brands),
            implode(', ', $categories),
            $product->get_price(),
            1,
            $search_string
        );

        $button_html = str_replace($search_string, $replace_string, $button_html);
    }
    return $button_html;
}

add_action( 'wp_footer', 'gv_ga_add_to_cart_loop' );
function gv_ga_add_to_cart_loop(){
  if(is_single()) return;
  ?>
  <script>
    (function($){
        $(document.body).on('added_to_cart', function( event, fragments, cart_hash, button ) {
            var product_id    = button.attr('data-ga_id'),
                product_name   = button.attr('data-ga_name'),
                product_brand   = button.attr('data-ga_brand'),
                product_category  = button.attr('data-ga_category'),
                product_price = button.attr('data-ga_price'),
                product_quantity = button.attr('data-ga_quantity');
            
            gtag('event', 'add_to_cart', {
                items: [
                    {
                        id: product_id,
                        name: product_name,
                        brand: product_brand,
                        category: product_category,
                        price: product_price,
                        quantity: product_quantity
                    }
                ]
            });
        });

    })(jQuery);
  </script>
  <?php
}

function gv_ga_set_add_to_cart_code($selector, $ga_code){
  ?>
  <script>
    (function($) {
      $( document.body ).on( 'click', '<?php echo $selector ?>', function(e) {
        <?php echo $ga_code ?>
      });
    })(jQuery);
  </script>
  <?php
}

// //Quitar del carrito
add_filter( 'woocommerce_cart_item_remove_link', 'filter_woocommerce_cart_item_remove_link', 10, 2 ); 
function filter_woocommerce_cart_item_remove_link( $remove_link, $cart_item_key ) {
    if(!is_cart()) return $remove_link;
    global $woocommerce;
    $item = $woocommerce->cart->get_cart_item($cart_item_key);
    $remove_link = str_replace('<a','<a data-product_id="'.$item['product_id'].'"', $remove_link);
    
    return $remove_link; 
}; 

add_action( 'woocommerce_after_cart', 'gv_ga_remove_from_cart' );
add_action( 'woocommerce_after_mini_cart', 'gv_ga_remove_from_cart' );
function gv_ga_remove_from_cart(){
  global $woocommerce;
  $items = $woocommerce->cart->get_cart();
  $cart = array();
  foreach($items as $item){
    $item_data = $item['data'];
    
    $categories = array();
    $category_terms =  get_the_terms($item['product_id'], 'product_cat');
    foreach($category_terms as $category){
        $categories []= $category->name;
    }

    $brands = array();
    $brand_terms =  get_the_terms($product->get_id(), 'brand') ? get_the_terms($product->get_id(), 'brand') : array();
    foreach($brand_terms as $brand){
        $brands []= $brand->name;
    }
    $_item = array(
      'id' => $item_data->get_sku() ? $item_data->get_sku() : ( '#' . $item['id'] ),
      'name' => $item_data->get_name(),
      'brand' => implode(', ', $brands),
      'category' => implode(', ', $categories),
      'quantity' =>  $item['quantity'],
      'price' => $item['line_total']
    );
    $cart[$item['product_id']] = $_item;
  }
  ?>
    <script>
    (function($) {
      cart_items = JSON.parse('<?php echo json_encode($cart); ?>');
      removed_items = [];
      $( document.body ).on( 'click', '.remove', function() {
        id = $(this).data('product_id');
        if(removed_items.includes(id)) return;
        console.log('remove');
        gtag( 'event', 'remove_from_cart', {
          'id' : cart_items[id]['id'],
          'name' : cart_items[id]['name'],
          'brand' : cart_items[id]['brand'],
          'category' : cart_items[id]['category'],
          'quantity' : cart_items[id]['quantity'],
          'price' : cart_items[id]['price']
        } );
        removed_items.push(id);
      });
    })(jQuery);
    </script>
 <?php
}

/**
 * begin_checkout
 */

add_action( 'woocommerce_after_cart', 'gv_ga_begin_checkout' );
function gv_ga_begin_checkout(){
  gv_set_checkout_ga_code('begin_checkout');
}
add_action( 'woocommerce_after_checkout_form', 'gv_ga_checkout_progress' );
function gv_ga_checkout_progress(){
  gv_set_checkout_ga_code('checkout_progress');
}

function gv_set_checkout_ga_code($event){
  global $woocommerce;
  $items = $woocommerce->cart->get_cart();
  $cart = array();
  foreach($items as $item){
    $item_data = $item['data'];
    
    $categories = array();
    $category_terms =  get_the_terms($item['product_id'], 'product_cat');
    foreach($category_terms as $category){
        $categories []= $category->name;
    }

    $brands = array();
    $brand_terms =  get_the_terms($product->get_id(), 'brand') ? get_the_terms($product->get_id(), 'brand') : array();
    foreach($brand_terms as $brand){
        $brands []= $brand->name;
    }

    $_item = array(
      'id' => $item_data->get_sku() ? $item_data->get_sku() : ( '#' . $item['id'] ),
      'name' => $item_data->get_name(),
      'brand' => implode(', ', $brands),
      'category' => implode(', ', $categories),
      'quantity' =>  $item['quantity'],
      'price' => $item['line_total']
    );
    $cart[] = $_item;
  }
  $ga_code = "gtag( 'event', '".$event."',  {";
  $ga_code .= $event == 'checkout_progress' ? "'checkout_step': 2," : '';
  $ga_code .= "'items':" . json_encode($cart);
  $ga_code .= "} );";
  echo '<script>'.$ga_code.'</script>';

}

// //Vista de detalle de producto
add_action( 'woocommerce_after_single_product', 'gv_ga_product_detail' );
function gv_ga_product_detail(){
  global $product;
  if ( empty( $product ) ) {
    return;
  }
  
  $categories = array();
  $category_terms =  get_the_terms($product->get_id(), 'product_cat');
  foreach($category_terms as $category){
    $categories []= $category->name;
  }

  $brands = array();
  $brand_terms =  get_the_terms($product->get_id(), 'brand') ? get_the_terms($product->get_id(), 'brand') : array();
  foreach($brand_terms as $brand){
    $brands []= $brand->name;
  }

  wc_enqueue_js( "
    gtag( 'event', 'view_item', {
      'id': '" . esc_js( $product->get_sku() ? $product->get_sku() : ( '#' . $product->get_id() ) ) . "',
      'name': '" . esc_js( $product->get_title() ) . "',
      'brand': '" . esc_js(implode(', ', $brands)) . "',
      'category': '" . esc_js(implode(', ', $categories)) . "',
      'price': '" . esc_js( $product->get_price() ) . "',
    } );" );
}