<?php

/** analytics */
require(get_stylesheet_directory().'/includes/analytics.php');

/** Cambios en tienda */
require(get_stylesheet_directory().'/includes/store-changes.php');

/** funciones de búsqueda */
require(get_stylesheet_directory().'/includes/search-results.php');

/** funciones de sincronización */
require(get_stylesheet_directory().'/includes/sync-functions.php');

/** funciones de ficha técnica */
require(get_stylesheet_directory().'/includes/products-uploads.php');

/** funciones de chat */
require(get_stylesheet_directory().'/includes/chat.php');

/** funciones de landing pages para marcas y categorías */
require(get_stylesheet_directory().'/includes/brand-landings.php');

/** Sobreescribir funciones de slider de productos */
require(get_stylesheet_directory().'/includes/etheme-slider.php');

add_action('wp_enqueue_scripts', 'gv_resources');
function gv_resources(){
    etheme_child_styles();
    if (is_front_page() || is_single() ) wp_dequeue_script('wc-cart-fragments');
}

//langs: themes and plugins
add_action( 'after_setup_theme', 'gv_theme_lang' );
function gv_theme_lang() {
    load_child_theme_textdomain( 'xstore', get_stylesheet_directory() . '/languages' );
    
    //plugins
    unload_textdomain('xstore-core');
    load_textdomain('xstore-core', get_stylesheet_directory() . '/languages/xstore-core-es_ES.mo');
}

/**
 * Reescribe etiquetas
 */
add_filter( 'gettext', 'gv_change_translation_strings', 20, 3 );
function gv_change_translation_strings( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
        case 'Guardar mi nombre, correo electrónico y web en este navegador para la próxima vez que comente.' :
            $translated_text = __( 'Guardar mi nombre y correo electrónico en este navegador para la próxima vez que comente.', $domain );
        break;
        case '(con impuestos)' :
            $translated_text = __( '(con IVA)', $domain );
        break;
        case 'Load More' :
            $translated_text = __( 'Cargar más', $domain );
        break;
        
    }
    return $translated_text;
}

/**
 * Gestión de roles shop_sales = ventas : dado de alta desde plugin
 */
add_action( 'wp_before_admin_bar_render', 'gv_admin_bar_remove_logo', 0 );
function gv_admin_bar_remove_logo() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu( 'wp-logo' );
}
add_action( 'admin_menu', 'gv_remove_menu_pages', 999);
function gv_remove_menu_pages() {
  global $current_user;
   
  $user_roles = $current_user->roles;
  $user_role = array_shift($user_roles);
  if($user_role == "shop_sales") {
    $remove_submenu = remove_submenu_page('woocommerce', 'wc-settings');
    $remove_submenu = remove_submenu_page('woocommerce', 'wc-addons');
  }
}

add_action('wp_footer', 'gv_add_script_data');
function gv_add_script_data(){
?>
	<script type="application/ld+json">
    {
        "@context" : "http://schema.org",
        "@type" : "Organization",
        "name" : "Garantía Verde",
        "url" : "https://garantiaverde.com.mx",
        "sameAs" : [
            "https://www.facebook.com/Gmartgarantiaverde/"
        ],
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "Blvd. Paseo Solidaridad #12574 Col. El Dorado",
            "addressRegion": "Irapuato, Gto., México",
            "postalCode": "36630",
            "addressCountry": "MX"
        }
    }
	</script>
<?php
}