jQuery(function($){
    /*
     * Select/Upload image(s) event
     */
    $('body').on('click', '.wc_upload_file', function(e){
        e.preventDefault();

            var button = $(this),
                custom_uploader = wp.media({
            title: 'Insertar documento',
            library : {
                // uncomment the next line if you want to attach image to the current post
                // uploadedTo : wp.media.view.settings.post.id, 
                type: [ 'audio', 'video', 'image', 'application/pdf' ]
            },
            button: {
                text: 'Usar' // button label text
            },
            multiple: false // for multiple image selection set to true
        }).on('select', function() { // it also has "open" and "close" events 
            var attachment = custom_uploader.state().get('selection').first().toJSON();
            $(button).removeClass('button').html('<p>' + attachment.filename + '</p>').next().val(attachment.id).next().show();
            /* if you sen multiple to true, here is some code for getting the image IDs
            var attachments = frame.state().get('selection'),
                attachment_ids = new Array(),
                i = 0;
            attachments.each(function(attachment) {
                attachment_ids[i] = attachment['id'];
                console.log( attachment );
                i++;
            });
            */
        })
        .open();
    });

    /*
     * Remove image event
     */
    $('body').on('click', '.wc_remove_file', function(){
        $(this).hide().prev().val('').prev().addClass('button').html('Subir archivo');
        return false;
    });

});