<?php

if (!class_exists('Openpay')) {
    // require_once("lib/openpay/Openpay.php");
    require_once("lib/bbva/Bbva.php");
}

/*
  Title:	Openpay Payment extension for WooCommerce
  Author:	Openpay
  URL:		http://www.openpay.mx
  License: GNU General Public License v3.0
  License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

class Openpay_Cards extends WC_Payment_Gateway
{

    protected $GATEWAY_NAME = "Openpay Cards";
    protected $is_sandbox = true;
    protected $order = null;
    protected $transaction_id = null;
    protected $transactionErrorMessage = null;
    protected $currencies = array('MXN', 'USD');
    protected $msi = array();
    protected $use_3d_secure = false;
    protected $deferred_months;

    public function __construct() {        
        $this->id = 'openpay_cards';
        $this->method_title = __('Openpay Cards', 'openpay_cards');
        $this->has_fields = true;

        $this->init_form_fields();
        $this->init_settings();
        $this->logger = wc_get_logger();
        
        

        $this->title = 'Pago con tarjeta de crédito o débito';
        $this->description = '';        
        $this->is_sandbox = strcmp($this->settings['sandbox'], 'yes') == 0;
        $this->test_merchant_id = $this->settings['test_merchant_id'];
        $this->test_private_key = $this->settings['test_private_key'];
        $this->test_publishable_key = $this->settings['test_publishable_key'];
        $this->test_affiliation_bbva = $this->settings['test_affiliation_bbva'];
        $this->live_merchant_id = $this->settings['live_merchant_id'];
        $this->live_private_key = $this->settings['live_private_key'];
        $this->live_publishable_key = $this->settings['live_publishable_key'];
        $this->live_affiliation_bbva = $this->settings['live_affiliation_bbva'];
        $this->merchant_id = $this->is_sandbox ? $this->test_merchant_id : $this->live_merchant_id;
        $this->publishable_key = $this->is_sandbox ? $this->test_publishable_key : $this->live_publishable_key;
        $this->affiliation_bbva = $this->is_sandbox ? $this->test_affiliation_bbva : $this->live_affiliation_bbva;
        $this->private_key = $this->is_sandbox ? $this->test_private_key : $this->live_private_key;
        $this->msi = $this->settings['msi'];
        $this->use_3d_secure = strcmp($this->settings['use_3d_secure'], 'yes') == 0;
        $this->deferred_months = $this->settings['deferred_months'];

        if ($this->is_sandbox) {
            $this->description .= __('SANDBOX MODE ENABLED. In test mode, you can use the card number 4111111111111111 with any CVC and a valid expiration date.', 'openpay-woosubscriptions');
        }
        
        if (!$this->validateCurrency()) {
            $this->enabled = false;
        }                        
        
        // https://developer.wordpress.org/reference/functions/add_action/
        add_action('wp_enqueue_scripts', array($this, 'payment_scripts'));
        add_action('woocommerce_update_options_payment_gateways_'.$this->id, array($this, 'process_admin_options'));
        add_action('admin_notices', array(&$this, 'perform_ssl_check'));        
        add_action('woocommerce_checkout_create_order', array($this, 'action_woocommerce_checkout_create_order'), 10, 2);                
    }       
    
    /**
     * Si el tipo de cargo esta configurado como Pre-autorización, el estatus de la orden es marcado como "on-hold"
     * 
     * @param type $order
     * @param type $data
     * 
     * @link https://docs.woocommerce.com/wc-apidocs/source-class-WC_Checkout.html#334
     */
    public function action_woocommerce_checkout_create_order($order, $data) {        
        if ($order->get_payment_method() == 'openpay_cards') {
            $order->set_status('pending');            
        }        
    } 
    
    public function perform_ssl_check() {
        if (!$this->is_sandbox && !is_ssl() && $this->enabled == 'yes') :
            echo '<div class="error"><p>'.sprintf(__('%s sandbox testing is disabled and can performe live transactions but the <a href="%s">force SSL option</a> is disabled; your checkout is not secure! Please enable SSL and ensure your server has a valid SSL certificate.', 'woothemes'), $this->GATEWAY_NAME, admin_url('admin.php?page=settings')).'</p></div>';
        endif;
    }

    public function init_form_fields() {
        $this->form_fields = array(
            'enabled' => array(
                'type' => 'checkbox',
                'title' => __('Habilitar módulo', 'woothemes'),
                'label' => __('Habilitar', 'woothemes'),
                'default' => 'yes'
            ),            
            'sandbox' => array(
                'type' => 'checkbox',
                'title' => __('Modo de pruebas', 'woothemes'),
                'label' => __('Habilitar', 'woothemes'),                
                'default' => 'no'
            ),
            'test_merchant_id' => array(
                'type' => 'text',
                'title' => __('ID de comercio de pruebas', 'woothemes'),
                'description' => __('Obten tus llaves de prueba de tu cuenta de Openpay.', 'woothemes'),
                'default' => __('', 'woothemes')
            ),
            'test_private_key' => array(
                'type' => 'text',
                'title' => __('Llave secreta de pruebas', 'woothemes'),
                'description' => __('Obten tus llaves de prueba de tu cuenta de Openpay ("sk_").', 'woothemes'),
                'default' => __('', 'woothemes')
            ),
            'test_publishable_key' => array(
                'type' => 'text',
                'title' => __('Llave pública de pruebas', 'woothemes'),
                'description' => __('Obten tus llaves de prueba de tu cuenta de Openpay ("pk_").', 'woothemes'),
                'default' => __('', 'woothemes')
            ),
            'live_merchant_id' => array(
                'type' => 'text',
                'title' => __('ID de comercio de producción', 'woothemes'),
                'description' => __('Obten tus llaves de producción de tu cuenta de Openpay.', 'woothemes'),
                'default' => __('', 'woothemes')
            ),
            'live_private_key' => array(
                'type' => 'text',
                'title' => __('Llave secreta de producción', 'woothemes'),
                'description' => __('Obten tus llaves de producción de tu cuenta de Openpay ("sk_").', 'woothemes'),
                'default' => __('', 'woothemes')
            ),
            'live_publishable_key' => array(
                'type' => 'text',
                'title' => __('Llave pública de producción', 'woothemes'),
                'description' => __('Obten tus llaves de producción de tu cuenta de Openpay ("pk_").', 'woothemes'),
                'default' => __('', 'woothemes')
            ),
            'test_affiliation_bbva' => array(
                'type' => 'text',
                'title' => __('Número de Afiliación BBVA de pruebas', 'woothemes'),
                'description' => __('Número de afiliación del comercio', 'woothemes'),
                'default' => __('', 'woothemes')
            ),              
            'live_affiliation_bbva' => array(
                'type' => 'text',
                'title' => __('Número de Afiliación BBVA de producción', 'woothemes'),
                'description' => __('Número de afiliación del comercio', 'woothemes'),
                'default' => __('', 'woothemes')
            ),
            'msi' => array(
                'title' => __('Meses sin intereses', 'woocommerce'),
                'type' => 'multiselect',
                'class' => 'wc-enhanced-select',
                'css' => 'width: 400px;',
                'default' => '',
                'options' => $this->getMsi(),
                'custom_attributes' => array(
                    'data-placeholder' => __('Opciones', 'woocommerce'),
                ),
            ),
            'deferred_months' => array(
                'title' => __('Meses diferidos', 'woocommerce'),
                'type' => 'select',
                'class' => 'wc-enhanced-select',
                'default' => '0',
                'desc_tip' => true,
                'options' => array(
                    '0' => __('No diferir', 'woocommerce'),
                    '3' => __('3 meses', 'woocommerce')
                ),
            ),
            'use_3d_secure' => array(
                'type' => 'checkbox',
                'title' => __('Usar 3D secure', 'woothemes'),
                'label' => __('Habilitar', 'woothemes'),
                'default' => 'no'
            ),  
        );
    }

    public function getMsi() {
        return array('3' => '3 meses', '6' => '6 meses', '9' => '9 meses', '12' => '12 meses', '18' => '18 meses');
    }

    public function admin_options() {
        include_once('templates/admin.php');
    }

    public function payment_fields() {
        //$this->images_dir = WP_PLUGIN_URL."/".plugin_basename(dirname(__FILE__)).'/assets/images/';    
        global $woocommerce;
        $months = array();          
        if(!empty($this->msi)){
            foreach ($this->msi as $msi) {
                $months[$msi] = $msi . ' meses';
            }
        }
        $this->months = $months;
        $this->images_dir = plugin_dir_url( __FILE__ ).'/assets/images/';
        include_once('templates/payment_vpos.php');
    }
    
    /**
     * payment_scripts function.
     *
     * Outputs scripts used for openpay payment
     *
     * @access public
     */
    public function payment_scripts() {
        //enqueue if neccesary
    }
    
    private function getProductsDetail() {
        $order = $this->order;
        $products = [];
        foreach( $order->get_items() as $item_product ){                        
            $product = $item_product->get_product();                        
            $products[] = $product->get_name();
        }
        return substr(implode(', ', $products), 0, 249);
    }

    public function process_payment($order_id) {
        global $woocommerce;
        $this->order = new WC_Order($order_id);
        $protocol = (!is_ssl()) ? 'http' : 'https';                             
        $redirect_url = site_url('/', $protocol).'wc-api/openpay_confirm';
        $amount = number_format((float)$this->order->get_total(), 2, '.', '');
        $bbva = $this->getBbvaInstance();

        $customer_data = array(
            'name' => $this->order->get_billing_first_name(),
            'last_name' => $this->order->get_billing_last_name(),
            'email' => $this->order->get_billing_email(),
            'phone_number' => $this->order->get_billing_phone()     
        );

        $chargeRequest = array(
            'affiliation_bbva' => $this->affiliation_bbva,
            'amount' => $amount,
            'description' => sprintf("%s", $this->getProductsDetail()),
            'currency' => strtoupper(get_woocommerce_currency()),
            'order_id' => $this->order->get_id(),
            'redirect_url' => $redirect_url,
            'customer' => $customer_data
        );

        if(!empty($this->msi) && $this->msi > 1 && isset($_POST['openpay_msi'])){
            $payment_plan = array(
                'payments' => $_POST['openpay_msi'],
                'payments_type' => 'WITHOUT_INTEREST',
            );
            if($this->deferred_months > 0){
                $payment_plan['deferred_months'] = $this->deferred_months;
            }

            $chargeRequest['payment_plan'] = $payment_plan;
        }

        if($this->use_3d_secure){
            $chargeRequest['use_3d_secure'] = "true";
        }

        try{
            $charge = $bbva->charges->create($chargeRequest);
            update_post_meta($this->order->get_id(), '_openpay_vpos_url', $charge->payment_method->url);
            update_post_meta($this->order->get_id(), '_openpay_transaction_id', $charge->id);
            $woocommerce->cart->empty_cart();            
            return array(
                'result' => 'success',
                'redirect' => $this->get_return_url($this->order)
            );
        } catch (BbvaApiTransactionError $e) {
            $error_log = array(
                'chargeRequest' => $chargeRequest,
                'message' => $e->getMessage(),
                'description' => $e->getDescription()
            );
            $this->logger->debug(print_r($error_log, true), array( 'source' => 'openpay_cards' ));  
            $this->error($e);
            return false;
        
        } catch (BbvaApiRequestError $e) {
            $error_log = array(
                'chargeRequest' => $chargeRequest,
                'message' => $e->getMessage()
            );
            $this->logger->debug(print_r($error_log, true), array( 'source' => 'openpay_cards' ));  
            $this->error($e);
            return false;
        
        } catch (BbvaApiConnectionError $e) {
            $error_log = array(
                'chargeRequest' => $chargeRequest,
                'message' => $e->getMessage()
            );
            $this->logger->debug(print_r($error_log, true), array( 'source' => 'openpay_cards' ));  
            $this->error($e);
            return false;
        
        } catch (BbvaApiAuthError $e) {
            $error_log = array(
                'chargeRequest' => $chargeRequest,
                'message' => $e->getMessage()
            );
            $this->logger->debug(print_r($error_log, true), array( 'source' => 'openpay_cards' ));  
            $this->error($e);
            return false;
            
        } catch (BbvaApiError $e) {
            $error_log = array(
                'chargeRequest' => $chargeRequest,
                'message' => $e->getMessage()
            );
            $this->logger->debug(print_r($error_log, true), array( 'source' => 'openpay_cards' ));  
            $this->error($e);
            return false;
            
        } catch (Exception $e) {
            $error_log = array(
                'chargeRequest' => $chargeRequest,
                'message' => $e->getMessage()
            );
            $this->logger->debug(print_r($error_log, true), array( 'source' => 'openpay_cards' ));  
            $this->error($e);
            return false;
        }
    }
    public function error(Exception $e) {
        global $woocommerce;

        /* 6001 el webhook ya existe */
        switch ($e->getCode()) {
            /* ERRORES GENERALES */
            case '1000':
            case '1004':
            case '1005':
                $msg = 'Servicio no disponible.';
                break;
            /* ERRORES TARJETA */
            case '3001':
            case '3004':
            case '3005':
            case '3007':
                $msg = 'La tarjeta fue rechazada.';
                break;
            case '3002':
                $msg = 'La tarjeta ha expirado.';
                break;
            case '3003':
                $msg = 'La tarjeta no tiene fondos suficientes.';
                break;
            case '3006':
                $msg = 'La operación no esta permitida para este cliente o esta transacción.';
                break;
            case '3008':
                $msg = 'La tarjeta no es soportada en transacciones en línea.';
                break;
            case '3009':
                $msg = 'La tarjeta fue reportada como perdida.';
                break;
            case '3010':
                $msg = 'El banco ha restringido la tarjeta.';
                break;
            case '3011':
                $msg = 'El banco ha solicitado que la tarjeta sea retenida. Contacte al banco.';
                break;
            case '3012':
                $msg = 'Se requiere solicitar al banco autorización para realizar este pago.';
                break;
            case 'custom':
                $msg = 'custom break';
                break;
            default: /* Demás errores 400 */
                $msg = 'La petición no pudo ser procesada.';
                break;
        }
        $error = 'ERROR '.$e->getErrorCode().'. '.$msg;        
        $this->transactionErrorMessage = $error;
        if (function_exists('wc_add_notice')) {
            wc_add_notice($error, $notice_type = 'error');
        } else {
            $woocommerce->add_error(__('Payment error:', 'woothemes').$error);
        }
    }

    public function debugError($e) {
        global $woocommerce;
        var_dump($e);
        
        if (function_exists('wc_add_notice')) {
            wc_add_notice('custom error', $notice_type = 'error');
        } else {
            $woocommerce->add_error(__('Payment error:', 'woothemes').'custom error');
        }
    }

    /**
     * Checks if woocommerce has enabled available currencies for plugin
     *
     * @access public
     * @return bool
     */
    public function validateCurrency() {
        return in_array(get_woocommerce_currency(), $this->currencies);
    }

    public function isNullOrEmptyString($string) {
        return (!isset($string) || trim($string) === '');
    }
    
    public function getBbvaInstance() {
        $bbva = Bbva::getInstance($this->merchant_id, $this->private_key);
        Bbva::setProductionMode($this->is_sandbox ? false : true);
        return $bbva;
    }

}

function openpay_cards_add_creditcard_gateway($methods) {
    array_push($methods, 'openpay_cards');
    return $methods;
}

add_filter('woocommerce_payment_gateways', 'openpay_cards_add_creditcard_gateway');

