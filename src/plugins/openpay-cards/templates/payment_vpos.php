<?php
/*
  Title:	Openpay Payment extension for WooCommerce
  Author:	Openpay
  URL:		http://www.openpay.mx
  License: GNU General Public License v3.0
  License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
?>
<style>
    .form-row{
        margin: 0 0 6px !important;
        padding: 3px !important;
    }
    
    .form-row select{
        width: 100% !important;
    }
</style>

<div style="overflow: hidden;">
    <div>
        <div style="width: 100%;">
            <h5 style="margin-bottom: 0">Tarjetas de crédito:</h5>	
            <img alt="" src="<?php echo $this->images_dir ?>credit_cards.png" style="display: block; margin-bottom: 1em">	
        </div>
        <div style="width: 100%;">
            <h5 style="margin-bottom: 0">Tarjetas de débito:</h5>	
            <img alt="" src="<?php echo $this->images_dir ?>debit_cards.png" style="display: block; margin-bottom: 1em">	
        </div>
    </div>	
    <div style="height: 1px; clear: both; border-bottom: 1px solid #CCC; margin: 10px 0 10px 0;"></div>
    <h4>Serás redireccionado a la plataforma de pago seguro de openpay.</h4>
    <?php if(!empty($this->months)): ?>
        <div class="form-row form-row-wide">
            <label for="openpay_msi">Pago a meses sin intereses <span class="required">*</span></label>
            <select name="openpay_msi" id="openpay_msi" class="form-control">
                <option value="1">Pago de contado</option>
                <?php foreach($this->months as $key => $month): ?>
                    <option value="<?php echo $key ?>"><?php echo $month ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    <?php endif; ?>
</div>
<div style="height: 1px; clear: both; border-bottom: 1px solid #CCC; margin: 10px 0 10px 0;"></div>
<div style="text-align: center">
    <img alt="" src="<?php echo $this->images_dir ?>openpay.png">	
</div>